
public class ComponentManager {
    private static ComponentManager man=null;
    public static ComponentInterface inter;
    private ComponentManager(ComponentInterface a){inter=a;}
    public static synchronized ComponentManager setComponentManager(ComponentInterface a){
    	if(man==null){ man=new ComponentManager(a);}
    	return man;
    }
    public static synchronized ComponentManager getComponentManager(){
    	return man;
    }
    
}
