# README #

This README would normally document whatever steps are necessary to get your Credential Manager up and running.

### What do you need to use the component? ###

* You need to import the following dependencies in your IDE:
    * Specs Utility parent Project (https://bitbucket.org/specs-team/specs-utility-specs_parent) 
* You need to convert the project to a Maven project (if your IDE doesn't recognize it as a Maven one);
* Execute Maven Install. 

### How to use the credential manager? ###

* This component is the one that communicate directly with the Vault Server. In fact, before using it it's necessary to configure a Vault Server on a VM. 
* The next step is to edit the configuration file (https://bitbucket.org/specs-team/specs-utility-credential-management-application/src/3d8d0e28f4bb43fccc5055de612ade2df20293d0/src/main/resources/vault_conf.properties?at=master&fileviewer=file-view-default) to make it compliant with the Vault Server instance.
* The last step consists into running this component so that it's possible to store new "data" inside the Vault Server. 

### Who do I talk to? ###

* Massimiliano Rak (Cerict) - maxrak@gmail.com 